import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lottie/lottie.dart';
import 'package:flutter_beacon/flutter_beacon.dart';
import 'package:permission_handler/permission_handler.dart';
// import 'dart:convert' as convert;
//import 'package:http/http.dart' as http;
// import 'dart:io' as io;
// import 'package:web_socket_channel/io.dart';
// import 'package:web_socket_channel/status.dart' as status;
import 'package:mqtt_client/mqtt_client.dart';
import 'package:mqtt_client/mqtt_server_client.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const VisieIoTApp());
}

// Main app and bootstrap
class VisieIoTApp extends StatelessWidget {
  const VisieIoTApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String appTitle = 'Visie Digital Life Platform';
    return MaterialApp(
      debugShowCheckedModeBanner: true,
      title: appTitle,
      home: const BeaconPage(),
      theme: ThemeData(
        primarySwatch: Colors.indigo,
        fontFamily: 'Prompt',
      ),
    );
  }
}

Future<void> showSnackBarNotification(
  BuildContext context,
  Widget content,
  SnackBarAction? action,
) async {
  final snackBar = SnackBar(
    content: content,
    action: action,
  );
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}

class BeaconPage extends StatefulWidget {
  const BeaconPage({Key? key}) : super(key: key);

  @override
  _BeaconPageState createState() => _BeaconPageState();
}

class _BeaconPageState extends State<BeaconPage> {
  StreamSubscription<RangingResult>? _streamRanging;
  final _beacons = <Beacon>[];
  final _regionBeacons = <Region, List<Beacon>>{};

  final MqttServerClient client =
      MqttServerClient('172.16.16.153', 'clientId1234'); //mqtt server & client ID
  final builder = MqttClientPayloadBuilder();

  @override
  void initState() {
    debugPrint("*** call INIT");
    _beacons.clear();
    super.initState();
    checkPermissonStatus();

    mqttConnect();
  }

  checkPermissonStatus() async {
    var bluetoothPermStatus = await Permission.bluetooth.status;
    var locationPermStatus = await Permission.location.status;
    debugPrint('Bluetooth permission: $bluetoothPermStatus');
    debugPrint('Location permission: $locationPermStatus');
    initScanBeacon();
  }

  initScanBeacon() async {
    debugPrint("*** Scan Beacon ***");
    try {
      await flutterBeacon.initializeAndCheckScanning;
    } on PlatformException catch (e) {
      debugPrint('Initialize flutter beacon error -> $e');
      showSnackBarNotification(
          context,
          const Text('Beacon scanner need Location and Bluetooth permission.'),
          null);
      return;
    }

    final regions = <Region>[];
    debugPrint("*** regions add ***");
    regions.add(
      Region(
        identifier: 'IBEACON',
        // proximityUUID: '5991E161-BB46-432F-9BD8-B271F76F67D9',
        // a4c9ff1d-e023-4e7e-8d28-1c2e449b932d
        proximityUUID: 'a4c9ff1d-e023-4e7e-8d28-1c2e449b932d',
      ),
    );
    if (_streamRanging != null) {
      if (_streamRanging!.isPaused) {
        _streamRanging?.resume();
        return;
      }
    }

    debugPrint("*** stream Ranging ***");
    _streamRanging =
        flutterBeacon.ranging(regions).listen((RangingResult result) {
      if (mounted) {
        setState(() {
          _regionBeacons[result.region] = result.beacons;
          _beacons.clear();
          for (var list in _regionBeacons.values) {
            _beacons.addAll(list);
          }

          _beacons.sort(_compareParameters);
        });
      }
    });
  }

  int _compareParameters(Beacon a, Beacon b) {
    // uuid compare
    int compare = a.proximityUUID.compareTo(b.proximityUUID);

    // accuracy compare
    if (compare == 0) {
      compare = a.accuracy.compareTo(b.accuracy);
    }

    // rssi compare
    if (compare == 0) {
      compare = a.rssi.compareTo(b.rssi);
    }

    // major
    if (compare == 0) {
      compare = a.major.compareTo(b.major);
    }

    // minor
    if (compare == 0) {
      compare = a.minor.compareTo(b.minor);
    }

    return compare;
  }

  @override
  void dispose() {
    _streamRanging?.cancel();
    super.dispose();
    debugPrint("/// debug dipose ///");
  }

  Future<bool> mqttConnect() async {
    client.logging(on: false);
    client.setProtocolV311();
    client.keepAlivePeriod = 20;
    client.port = 1883;
    client.onDisconnected = onDisconnected;
    client.onConnected = onConnected;
    client.pongCallback = pong;
    client.autoReconnect = true;
    client.websocketProtocols = MqttClientConstants.protocolsSingleDefault;

    final MqttConnectMessage conMess = MqttConnectMessage()
        .withClientIdentifier("clientId_nakrub2")
        .startClean();
    debugPrint("!!! MQTT client connceting ...");
    client.connectionMessage = conMess;

    try {
      await client.connect('ruuruu', '1234'); //mqtt username & password
    } on Exception catch (e) {
      debugPrint('EXAMPLE::client exception - $e');
      client.disconnect();
      return false;
    }

    if (client.connectionStatus!.state == MqttConnectionState.connected) {
      debugPrint("Connected MQTT successfully !!");
    } else {
      debugPrint("MQTT Connection Failed !!!!!");
      return false;
    }

    return false;
  }

  void onDisconnected() {
    debugPrint(
        'EXAMPLE::OnDisconnected client callback - Client disconnection');
    if (client.connectionStatus!.disconnectionOrigin ==
        MqttDisconnectionOrigin.solicited) {
      debugPrint(
          'EXAMPLE::OnDisconnected callback is solicited, this is correct');
    }
  }

  void onConnected() {
    debugPrint(
        'EXAMPLE::OnConnected client callback - Client connection was sucessful');
  }

  void pong() {
    debugPrint('EXAMPLE::Ping response client callback invoked');
  }

  @override
  Widget build(BuildContext context) {
    //debugPrint("*** debug build ***");
    // debugPrint(_beacons.toString());
    // debugPrint(_regionBeacons.toString());
    /// Publish it
    ///

    String userId = 'John123';
    String _beaconLocation = _beacons.toString();
    String payload = '{"userId":"$userId", "beacon":$_beaconLocation}';
    if (_beacons.isNotEmpty) {
      builder.addString(payload);
      debugPrint('EXAMPLE::Publishing our topic');
      client.publishMessage(
          "@msg/beacon", MqttQos.atMostOnce, builder.payload!);

      debugPrint('Beacon: ${_beacons.length} devices');
    } else {
      debugPrint('*** Not found beacon');
    }
    builder.clear(); //clear builder
    String? mqttState = client.connectionStatus?.state.toString();
    return Scaffold(
        appBar: AppBar(title: Text('iBeacon scanner (${_beacons.length})')),
        body: Column(
          children: <Widget>[
            Text('Status: $mqttState'),
            //Text('Craft beautiful UIs'),
            Expanded(
              child: _beacons.isEmpty
                  ? Lottie.asset('assets/lotties/finding.json')
                  : ListView(
                      children: ListTile.divideTiles(
                        context: context,
                        tiles: _beacons.map(
                          (beacon) {
                            return ListTile(
                              title: Text(
                                beacon.macAddress ?? '',
                                style: const TextStyle(fontSize: 15.0),
                              ),
                              subtitle: Column(
                                children: [
                                  Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      Text(
                                        'UUID: ${beacon.proximityUUID}',
                                        style: const TextStyle(fontSize: 13.0),
                                      )
                                    ],
                                  ),
                                  Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      Flexible(
                                        child: Text(
                                          'Major: ${beacon.major}\nMinor: ${beacon.minor}',
                                          style:
                                              const TextStyle(fontSize: 13.0),
                                        ),
                                        flex: 1,
                                        fit: FlexFit.tight,
                                      ),
                                      Flexible(
                                        child: Text(
                                          'Accuracy: ${beacon.accuracy}m\nRSSI: ${beacon.rssi}',
                                          style:
                                              const TextStyle(fontSize: 13.0),
                                        ),
                                        flex: 2,
                                        fit: FlexFit.tight,
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            );
                          },
                        ),
                      ).toList(),
                    ),
            ),
          ],
        ));
  }
}
